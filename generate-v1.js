const R = require("ramda");
const fs = require("file-system");
const psgcJsonData = require("./psgc.json");
//const publicationDateFolder = "3112019";

/* util */
function createKeyValueObject(arrayKeys, arrayValues) {
  return R.map(R.zipObj(arrayKeys), arrayValues);
}

function writeFile(path, content) {
  const stringifiedContent = JSON.stringify(content);
  fs.writeFileSync(`${path}`, stringifiedContent);
  //fs.writeFileSync(`${publicationDateFolder}/${path}`, stringifiedContent);
}

function filterPSGCJsonData(filter) {
  return R.filter(filter, psgcJsonData);
}

function generateAPIfromData(data, functionToGenerate, filepath) {
  const getCodeThenSort = R.compose(
    R.sortBy(parseInt),
    R.map(c => c.code)
  );
  const codes = getCodeThenSort(data);
  //console.log(codes);
  console.log("codes to generate", codes.length);

  mapIndexed((code, idx) => {
    //console.log(codes[idx], codes[idx + 1]);

    const greaterThanCode = R.gt(R.__, parseInt(codes[idx]));
    const lessThanCode = codes[idx + 1]
      ? R.lt(R.__, parseInt(codes[idx + 1]))
      : R.T;

    const filterCode = { code: R.allPass([greaterThanCode, lessThanCode]) };

    functionToGenerate(filterCode, `${filepath}/${codes[idx]}/index.html`);
  }, codes);
}

function generateAPIfromDataWithMaxCode(data, functionToGenerate, filepath) {
  const getCodeThenSort = R.compose(
    R.sortBy(parseInt),
    R.map(c => c.code)
  );
  const codes = getCodeThenSort(data);
  //console.log(codes);
  console.log(codes.length);

  console.log("codes to generate", codes.length);

  mapIndexed((code, idx) => {
    console.log(codes[idx], codes[idx + 1]);

    let maxCode = codes[idx + 1];

    if (!codes[idx + 1]) {
      const first4Digits = parseInt(`${R.take(4, codes[idx].toString())}00000`);
      maxCode = first4Digits + 100000;
      //console.log(R.take(4, R.toString(codes[idx])), first4Digits, maxCode);
    }

    const greaterThanCode = R.gt(R.__, parseInt(codes[idx]));
    const lessThanCode = R.lt(R.__, maxCode);

    const filterCode = { code: R.allPass([greaterThanCode, lessThanCode]) };

    functionToGenerate(filterCode, `${filepath}/${codes[idx]}/index.html`);
  }, codes);
}

function generateAPIfromDataWithMaxCodeForDistrict(
  data,
  functionToGenerate,
  filepath
) {
  const getCodeThenSort = R.compose(
    R.sortBy(parseInt),
    R.map(c => c.code)
  );
  const codes = getCodeThenSort(data);
  //console.log(codes);
  console.log(codes.length);

  console.log("codes to generate", codes.length);

  mapIndexed((code, idx) => {
    console.log(codes[idx], codes[idx + 1]);

    let maxCode = codes[idx + 1];

    if (!codes[idx + 1]) {
      const first4Digits = parseInt(`${R.take(4, codes[idx].toString())}00000`);
      maxCode = first4Digits + 100000;
      // console.log(
      //   codes[idx].toString(),
      //   R.take(4, codes[idx].toString()),
      //   first4Digits,
      //   maxCode
      // );
    }

    const greaterThanCode = R.gte(R.__, parseInt(codes[idx]));
    const lessThanCode = R.lt(R.__, maxCode);

    const filterCode = { code: R.allPass([greaterThanCode, lessThanCode]) };

    functionToGenerate(filterCode, `${filepath}/${codes[idx]}/index.html`);
  }, codes);
}

const mapIndexed = R.addIndex(R.map);
const isNotEmpty = R.complement(R.isEmpty);

/* generate function */
function generateIslandGroups() {
  const islandGroup = [
    ["luzon", "Luzon"],
    ["visayas", "Visayas"],
    ["mindanao", "Mindanao"]
  ];
  const arrayKeys = ["code", "name"];
  const data = createKeyValueObject(arrayKeys, islandGroup);

  writeFile("island-groups/index.html", data);

  const luzon = [
    130000000,
    140000000,
    "010000000",
    "020000000",
    "030000000",
    "040000000",
    170000000,
    "050000000"
  ];
  const visayas = ["060000000", "070000000", "080000000"];
  const mindanao = [
    "090000000",
    100000000,
    110000000,
    120000000,
    160000000,
    150000000
  ];

  const filter = (codeArr, geographicLevel, addedFilter = {}) =>
    R.where(
      R.merge(
        {
          geographicLevel: R.equals(geographicLevel),
          code: R.contains(R.__, codeArr)
        },
        addedFilter
      )
    );
  const filterData = filter => filterPSGCJsonData(filter);
  const islandGroupFile = (codeArr, path) => {
    let provinces = [];
    let cities = [];
    let municipalities = [];
    let barangays = [];
    let districts = [];
    let subMunicipalities = [];

    const generateFilter = (geographicLevel, code) =>
      R.where({
        geographicLevel: R.equals(geographicLevel),
        code: R.allPass(code)
      });

    R.map(code => {
      const maxCode = parseInt(code) + 10000000;
      const greaterThanCode = R.gt(R.__, parseInt(code));
      const lessThanCode = R.lt(R.__, maxCode);

      const provinceData = filterPSGCJsonData(
        generateFilter("Prov", [greaterThanCode, lessThanCode])
      );
      const cityData = filterPSGCJsonData(
        generateFilter("City", [greaterThanCode, lessThanCode])
      );
      const districtData = filterPSGCJsonData(
        generateFilter("Dist", [greaterThanCode, lessThanCode])
      );
      const municipalityData = filterPSGCJsonData(
        generateFilter("Mun", [greaterThanCode, lessThanCode])
      );
      const barangayData = filterPSGCJsonData(
        generateFilter("Bgy", [greaterThanCode, lessThanCode])
      );
      const subMunicipalityData = filterPSGCJsonData(
        generateFilter("SubMun", [greaterThanCode, lessThanCode])
      );
      const removeNotAProvince = {
        name: R.replace(' (Not a Province)', ''),
      };

      provinces = R.concat(provinces, provinceData);
      cities = R.concat(cities, cityData);
      municipalities = R.concat(municipalities, municipalityData);
      barangays = R.concat(barangays, barangayData);
      districts = R.map(R.evolve(removeNotAProvince), R.concat(districts, districtData));
      subMunicipalities = R.concat(subMunicipalities, subMunicipalityData);
    }, codeArr);

    writeFile(`island-groups/${path}/provinces/index.html`, provinces);
    writeFile(`island-groups/${path}/cities/index.html`, cities);
    writeFile(`island-groups/${path}/districts/index.html`, districts);
    writeFile(
      `island-groups/${path}/municipalities/index.html`,
      municipalities
    );
    writeFile(`island-groups/${path}/barangays/index.html`, barangays);
    writeFile(
      `island-groups/${path}/sub-municipalities/index.html`,
      subMunicipalities
    );
  };

  const filterLuzonProv = filter(luzon, "Reg");
  let luzonData = filterData(filterLuzonProv);
  writeFile("island-groups/luzon/regions/index.html", luzonData);
  islandGroupFile(luzon, "luzon");

  const filterVisayasProv = filter(visayas, "Reg");
  let visayasData = filterData(filterVisayasProv);
  writeFile("island-groups/visayas/regions/index.html", visayasData);
  islandGroupFile(visayas, "visayas");

  const filterMindanaoProv = filter(mindanao, "Reg");
  const mindanaoData = filterData(filterMindanaoProv);
  writeFile("island-groups/mindanao/regions/index.html", mindanaoData);
  islandGroupFile(mindanao, "mindanao");
}

function generateRegions() {
  const filter = R.propEq("geographicLevel", "Reg");
  const data = filterPSGCJsonData(filter);

  writeFile("regions/index.html", data);

  console.log("Generating Provinces...");
  generateAPIfromData(data, generateProvinces, "provinces");

  console.log("Generating Districts...");
  generateAPIfromData(data, generateDistricts, "districts");
}

function generateProvinces(
  addedFilter = {},
  pathToWrite = "provinces/index.html"
) {
  const filter = R.where(
    R.merge({ geographicLevel: R.equals("Prov") }, addedFilter)
  );
  const data = filterPSGCJsonData(filter);

  writeFile(pathToWrite, data);
  //console.log(data);

  if (isNotEmpty(addedFilter)) {
    console.log("Generating Cities...");
    generateAPIfromDataWithMaxCode(data, generateCities, "cities");
    console.log("Generating Municipalities...");
    generateAPIfromDataWithMaxCode(
      data,
      generateMunicipalities,
      "municipalities"
    );
    console.log("Generating Provice Sub Municipalities...");
    generateAPIfromDataWithMaxCode(
      data,
      generateSubMunicipalities,
      "sub-municipalities"
    );
  }
}

function generateCities(addedFilter = {}, pathToWrite = "cities/index.html") {
  const filter = R.where(
    R.merge({ geographicLevel: R.equals("City") }, addedFilter)
  );
  const data = filterPSGCJsonData(filter);
  // console.log(data, filter);

  writeFile(pathToWrite, data);

  if (isNotEmpty(addedFilter)) {
    console.log("Generating City Barangays...");
    generateAPIfromDataWithMaxCode(data, generateBarangays, "barangays");
  }
}

function generateMunicipalities(
  addedFilter = {},
  pathToWrite = "municipalities/index.html"
) {
  const filter = R.where(
    R.merge({ geographicLevel: R.equals("Mun") }, addedFilter)
  );
  const data = filterPSGCJsonData(filter);
  //console.log("called", data.length);

  writeFile(pathToWrite, data);

  if (isNotEmpty(addedFilter)) {
    console.log("Generating Municipality Barangays...");
    generateAPIfromDataWithMaxCode(data, generateBarangays, "barangays");
  }
}

function generateBarangays(
  addedFilter = {},
  pathToWrite = "barangays/index.html"
) {
  const filter = R.where(
    R.merge({ geographicLevel: R.equals("Bgy") }, addedFilter)
  );
  const data = filterPSGCJsonData(filter);

  writeFile(pathToWrite, data);
}

function generateDistricts(
  addedFilter = {},
  pathToWrite = "districts/index.html"
) {
  const filter = R.where(
    R.merge({ geographicLevel: R.equals("Dist") }, addedFilter)
  );
  const data = filterPSGCJsonData(filter);
  const removeNotAProvince = {
    name: R.replace(' (Not a Province)', ''),
  };
  const cleanedData = R.map(R.evolve(removeNotAProvince), data);

  writeFile(pathToWrite, cleanedData);

  if (isNotEmpty(addedFilter)) {
    console.log("Generating District Cities...");
    generateAPIfromDataWithMaxCodeForDistrict(data, generateCities, "cities");
    console.log("Generating Municipalities...");
    generateAPIfromDataWithMaxCode(
      data,
      generateMunicipalities,
      "municipalities"
    );
    console.log("Generating District Sub Municipalities...");
    generateAPIfromDataWithMaxCode(
      data,
      generateSubMunicipalities,
      "sub-municipalities"
    );
  }
}

function generateSubMunicipalities(
  addedFilter = {},
  pathToWrite = "sub-municipalities/index.html"
) {
  const filter = R.where(
    R.merge({ geographicLevel: R.equals("SubMun") }, addedFilter)
  );
  const data = filterPSGCJsonData(filter);

  writeFile(pathToWrite, data);

  console.log("Generating Sub-municipality Barangays...");
  generateAPIfromDataWithMaxCode(data, generateBarangays, "barangays");
}

console.log("Generating Island Groups...");
generateIslandGroups();
console.log("Generating Regions...");
generateRegions();
generateProvinces();
generateCities();
generateMunicipalities();
generateBarangays();
console.log("Generating Districts...");
generateDistricts();
console.log("Generating Sub Municipalities...");
generateSubMunicipalities();

console.log("Generated...");
